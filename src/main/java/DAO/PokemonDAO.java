package dao;

import mapper.PokemonMapper;
import model.Pokemon;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

/**
 * A lightweight interface for handling SQL queries.
 */
@RegisterMapper(PokemonMapper.class)
public interface PokemonDAO {
    @SqlUpdate("CREATE TABLE IF NOT EXISTS pokemon (\n" +
            "  id INTEGER PRIMARY KEY NOT NULL,\n" +
            "  name CHARACTER VARYING(32) NOT NULL,\n" +
            "  height INTEGER NOT NULL,\n" +
            "  weight INTEGER NOT NULL,\n" +
            "  color_id INTEGER NOT NULL,\n" +
            "  evolves_from_id INTEGER\n" +
            ");")
    void createTableIfNotExists();

    @SqlUpdate("INSERT INTO pokemon VALUES (:id, :name, :height, :weight, :color_id, :evolves_from_id)")
    void insert(@Bind("id") int id,
                @Bind("name") String name,
                @Bind("height") int height,
                @Bind("weight") int weight,
                @Bind("color_id") int color_id,
                @Bind("evolves_from_id") Integer evolves_from_id);

    @SqlUpdate("DROP TABLE IF EXISTS pokemon")
    void dropTable();

    @SqlUpdate("DELETE FROM pokemon WHERE id = :id;")
    void deleteById(@Bind("id") int id);

    @SqlQuery("SELECT * FROM pokemon")
    List<Pokemon> getAllPokemon();

    @SqlQuery("SELECT * FROM pokemon WHERE id = :id")
    Pokemon getById(@Bind("id") int id);

    @SqlQuery("SELECT * FROM pokemon WHERE name = :name")
    Pokemon getByName(@Bind("name") String name);

    void close();
}
