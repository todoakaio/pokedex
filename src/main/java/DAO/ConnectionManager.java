package dao;

import static com.heroku.sdk.jdbc.DatabaseUrl.extract;

import org.skife.jdbi.v2.tweak.ConnectionFactory;

import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Can return an open connection to the database.
 */
public class ConnectionManager implements ConnectionFactory {

    /**
     * Opens a connection with the database.
     *
     * @return an open connection to the database.
     * @throws SQLException if there are problems.
     */
    public Connection openConnection() throws SQLException {
        try {
            return extract().getConnection();
        } catch (URISyntaxException e) {
            throw new RuntimeException("Trouble getting the connection. ", e);
        }
    }
}
