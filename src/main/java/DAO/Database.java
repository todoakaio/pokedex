package dao;

import com.google.inject.Inject;
import lombok.experimental.Delegate;
import org.skife.jdbi.v2.DBI;

/**
 * The interface for retrieving the DAO from the database.  For example,
 * database.onDemand(PokemonDAO.class).dropTable();
 */
public class Database {

    @Delegate
    private DBI dbi;

    @Inject
    public Database(ConnectionManager connectionManager) {
        dbi = new DBI(connectionManager);
    }
}
