import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import controller.PokedexServer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Starts the server and serves up requests.
 */
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class Main implements Runnable {

    private final PokedexServer pokedexServer;

    public static void main(String[] args) {
        Injector injector = Guice.createInjector();
        injector.getInstance(Main.class).run();
    }

    @Override
    public void run() {
        log.info("Starting...");
        pokedexServer.setUpRoutes();
    }
}
