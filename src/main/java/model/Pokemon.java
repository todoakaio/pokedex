package model;

import lombok.Builder;
import lombok.Data;

/**
 * Data object for a Pokemon.
 */
@Builder
@Data
public class Pokemon {
    private int id;
    private String name;
    private int height;
    private int weight;
    private String color;
    private int evolves_from_id;
}
