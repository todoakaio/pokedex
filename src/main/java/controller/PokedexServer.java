package controller;

import static spark.Spark.*;

import com.google.inject.Inject;
import dao.Database;
import lombok.RequiredArgsConstructor;
import routes.*;
import spark.ModelAndView;
import spark.template.freemarker.FreeMarkerEngine;

/**
 * Initializes the server according to predefined routes.
 */
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class PokedexServer {

    private final GetPokemonRoute getPokemonRoute;
    private final SearchRoute searchRoute;
    private final InitTablesRoute initTablesRoute;
    private final GetAllPokemonRoute getAllPokemonRoute;
    private final DeleteAllPokemonRoute deleteAllPokemonRoute;
    private final DeletePokemonRoute deletePokemonRoute;
    private final PutPokemonRoute putPokemonRoute;
    private final Database database;

    /**
     * Sets up the server.
     */
    public void setUpRoutes() {
        port(Integer.valueOf(System.getenv("PORT")));
        staticFileLocation("/public");

        setUpHelloRoutes();
        setUpRootRoute();
        setUpPokemonRoutes();
        setUpSearchRoute();
    }

    private void setUpRootRoute() {
        get("/", (request, response) -> new ModelAndView(null, "index.ftl"), new FreeMarkerEngine());
    }

    private void setUpHelloRoutes() {
        get("/hello", (req, res) -> "Hello World!");
        get("/hello/:name", (request, response) -> "Hello, " + request.params(":name") + "!");
    }

    private void setUpPokemonRoutes() {
        get("/rest", (request, response) -> new ModelAndView(null, "rest.ftl"), new FreeMarkerEngine());

        get("/pokemon", getAllPokemonRoute, new FreeMarkerEngine());
        get("/pokemon/", getAllPokemonRoute, new FreeMarkerEngine());
        delete("/pokemon", deleteAllPokemonRoute, new FreeMarkerEngine());

        get("/pokemon/edit", (request, response) -> new ModelAndView(null, "edit.ftl"), new FreeMarkerEngine());
        get("/pokemon/delete", deleteAllPokemonRoute, new FreeMarkerEngine());
        get("/pokemon/init", initTablesRoute, new FreeMarkerEngine());
        post("/pokemon/init", initTablesRoute, new FreeMarkerEngine());
        post("/pokemon/edit", putPokemonRoute, new FreeMarkerEngine());

        get("/pokemon/:id", getPokemonRoute, new FreeMarkerEngine());
        put("/pokemon/:id", putPokemonRoute, new FreeMarkerEngine());
        delete("/pokemon/:id", deletePokemonRoute, new FreeMarkerEngine());
    }

    private void setUpSearchRoute() {
        get("/search", searchRoute, new FreeMarkerEngine());
    }
}
