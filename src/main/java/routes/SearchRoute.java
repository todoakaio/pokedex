package routes;

import com.google.inject.Inject;
import dao.Database;
import dao.PokemonDAO;
import lombok.RequiredArgsConstructor;
import model.Pokemon;
import org.apache.commons.lang3.StringUtils;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Route for handling search queries.
 */
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class SearchRoute implements TemplateViewRoute {

    private final Database database;
    private final String QUERY_KEY = "q";

    @Override
    public ModelAndView handle(Request request, Response response) {
        String searchRequest = "";
        List<Pokemon> results = new ArrayList<>();
        if (request.queryMap(QUERY_KEY).hasValue()) {
            database.onDemand(PokemonDAO.class).createTableIfNotExists();
            searchRequest = request.queryMap(QUERY_KEY).value();
            if (StringUtils.isNumeric(searchRequest)) {
                searchById(results, searchRequest);
            } else if (StringUtils.isNotBlank(searchRequest)) {
                searchByString(results, searchRequest);
            }
        }

        if (!results.isEmpty()) {
            return createSearchResultsView(results, searchRequest);
        } else {
            return createNoResultsView(searchRequest);
        }
    }

    private void searchById(List<Pokemon> results, String searchRequest) {
        Pokemon pokemon = database.onDemand(PokemonDAO.class).getById(Integer.valueOf(searchRequest));
        if (pokemon != null) {
            results.add(pokemon);
        }
    }

    private void searchByString(List<Pokemon> results, String searchRequest) {
        Pokemon pokemon = database.onDemand(PokemonDAO.class).getByName(StringUtils.lowerCase(searchRequest).replace(' ', '-'));
        if (pokemon != null) {
            results.add(pokemon);
        } else {
            List<Pokemon> all = database.onDemand(PokemonDAO.class).getAllPokemon();
            int threshold = 1;
            while (results.isEmpty() && threshold < 5) {
                for (Pokemon current : all) {
                    int levenshteinDistance = StringUtils.getLevenshteinDistance(current.getName(), searchRequest, threshold);
                    if (levenshteinDistance > 0) {
                        results.add(current);
                    }
                }
                threshold++;
            }
        }
    }

    private ModelAndView createSearchResultsView(List<Pokemon> results, String searchRequest) {
        Map<String, Object> attributes = new HashMap<>();
        attributes.put("allPokemon", results);
        attributes.put("searchRequest", searchRequest);
        return new ModelAndView(attributes, "search.ftl");
    }

    private ModelAndView createNoResultsView(String searchRequest) {
        Map<String, String> attributes = new HashMap<>();
        attributes.put("error", "Sorry, no Pokemon were found with your request, \"" + searchRequest + "\".");
        return new ModelAndView(attributes, "index.ftl");
    }
}
