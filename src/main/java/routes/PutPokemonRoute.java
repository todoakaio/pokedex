package routes;

import com.google.inject.Inject;
import dao.Database;
import dao.PokemonDAO;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

import java.util.HashMap;
import java.util.Map;

/**
 * Route for putting a Pokemon in the database.
 */
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class PutPokemonRoute implements TemplateViewRoute {

    private static final String NAME_KEY = "name";
    private static final String HEIGHT_KEY = "height";
    private static final String WEIGHT_KEY = "weight";
    private static final String COLOR_KEY = "color";
    private static final String EVOLVES_FROM_KEY = "evolves_from";
    private final Database database;

    @Override
    public ModelAndView handle(Request request, Response response) {
        if (!request.queryMap().get(NAME_KEY).hasValue() ||
                !request.queryMap().get(HEIGHT_KEY).hasValue() || !StringUtils.isNumeric(request.queryMap(HEIGHT_KEY).value()) ||
                !request.queryMap().get(WEIGHT_KEY).hasValue() || !StringUtils.isNumeric(request.queryMap(WEIGHT_KEY).value()) ||
                !request.queryMap().get(COLOR_KEY).hasValue() || !StringUtils.isNumeric(request.queryMap(COLOR_KEY).value())) {
            return createFailureView();
        }

        PokemonDAO dao = database.open(PokemonDAO.class);

        int id;
        if (request.params(":id") != null) {
            id = Math.abs(Integer.valueOf(request.params(":id").replace(",", "")));
        } else if (request.queryMap("id").hasValue()) {
            id = request.queryMap("id").integerValue();
        } else {
            return createFailureView();
        }

        boolean updated = deleteExistingRecord(dao, id, request);

        dao.insert(
                id,
                request.queryMap(NAME_KEY).value(),
                request.queryMap(HEIGHT_KEY).integerValue(),
                request.queryMap(WEIGHT_KEY).integerValue(),
                request.queryMap(COLOR_KEY).integerValue(),
                request.queryMap(EVOLVES_FROM_KEY).hasValue() && StringUtils.isNumeric(request.queryMap(EVOLVES_FROM_KEY).value()) ?
                        request.queryMap(EVOLVES_FROM_KEY).integerValue() : null
        );
        dao.close();

        return createSuccessView(request, id, updated);
    }

    private boolean deleteExistingRecord(PokemonDAO dao, int id, Request request) {
        dao.createTableIfNotExists();
        if (dao.getById(id) != null) {
            dao.deleteById(id);
            return true;
        }
        return false;
    }

    private ModelAndView createFailureView() {
        Map<String, String> attributes = new HashMap<>();
        attributes.put("error", "Not enough or incorrect information was passed in to your new Pokemon.");
        return new ModelAndView(attributes, "index.ftl");
    }

    private ModelAndView createSuccessView(Request request, int id, boolean updated) {
        Map<String, Object> attributes = new HashMap<>();
        String action = updated ? "Updated " : "Created ";
        attributes.put("message", action + request.queryMap(NAME_KEY).value() + "!  " +
                "<b><a href=\"/pokemon/" + id + "\">Check it out!</href></b>");
        return new ModelAndView(attributes, "index.ftl");
    }

}
