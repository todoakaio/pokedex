package routes;

import com.google.inject.Inject;
import dao.Database;
import dao.PokemonDAO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Route for initializing the Pokemon table in the database.
 */
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class InitTablesRoute implements TemplateViewRoute {

    private final String POKEMON_CSV_PATH = "data/pokemon.csv";
    private final Database database;

    @Override
    public ModelAndView handle(Request request, Response response) {
        File source = new File(POKEMON_CSV_PATH);
        try {
            Scanner scanner = new Scanner(source);
            scanner.nextLine();
            int count = 0;
            PokemonDAO dao = database.open(PokemonDAO.class);
            dao.dropTable();
            dao.createTableIfNotExists();
            while (scanner.hasNextLine()) {
                String[] values = scanner.nextLine().split(",");
                dao.insert(
                        Integer.valueOf(values[0]),
                        values[1],
                        Integer.valueOf(values[2]),
                        Integer.valueOf(values[3]),
                        Integer.valueOf(values[4]),
                        values.length > 5 ? Integer.valueOf(values[5]) : null);
                count++;
            }
            dao.close();
            Map<String, Object> attributes = new HashMap<>();
            attributes.put("message", "Created " + count + " records in the Pokemon table.");
            return new ModelAndView(attributes, "index.ftl");
        } catch (FileNotFoundException e) {
            Map<String, String> attributes = new HashMap<>();
            attributes.put("error", "Could not find local file : " + source.getAbsolutePath() + ".");
            return new ModelAndView(attributes, "index.ftl");
        }
    }
}
