package routes;

import com.google.inject.Inject;
import dao.Database;
import dao.PokemonDAO;
import lombok.RequiredArgsConstructor;
import model.Pokemon;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

import java.util.HashMap;
import java.util.Map;

/**
 * Route for getting and showing a single Pokemon.
 */
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class GetPokemonRoute implements TemplateViewRoute {

    private final Database database;

    @Override
    public ModelAndView handle(Request request, Response response) {
        Map<String, Object> attributes = new HashMap<>();
        database.onDemand(PokemonDAO.class).createTableIfNotExists();
        Pokemon pokemon = database.onDemand(PokemonDAO.class).getById(Integer.valueOf(request.params(":id").replace(",", "")));
        if (pokemon != null) {
            attributes.put("pokemon", pokemon);

            putEvolvesFromAttribute(attributes, pokemon);
            putPreviousPokemonAttribute(request, attributes);
            putFollowingPokemonAttribute(request, attributes);

            return new ModelAndView(attributes, "pokemon.ftl");
        } else {
            attributes.put("error", "No Pokemon found with id: " + request.params(":id"));
            return new ModelAndView(attributes, "index.ftl");
        }
    }

    private void putFollowingPokemonAttribute(Request request, Map<String, Object> attributes) {
        Pokemon pokemon = database.onDemand(PokemonDAO.class).getById(Integer.valueOf(request.params(":id").replace(",", "")) + 1);
        if (pokemon != null) {
            attributes.put("pokemonAfter", pokemon);
        }
    }

    private void putPreviousPokemonAttribute(Request request, Map<String, Object> attributes) {
        Pokemon pokemon = database.onDemand(PokemonDAO.class).getById(Integer.valueOf(request.params(":id").replace(",", "")) - 1);
        if (pokemon != null) {
            attributes.put("pokemonBefore", pokemon);
        }
    }

    private void putEvolvesFromAttribute(Map<String, Object> attributes, Pokemon pokemon) {
        Pokemon evolvesFrom = database.onDemand(PokemonDAO.class).getById(pokemon.getEvolves_from_id());
        if (evolvesFrom != null) {
            attributes.put("evolves_from", evolvesFrom);
        }
    }
}
