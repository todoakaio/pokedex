package routes;

import com.google.inject.Inject;
import dao.Database;
import dao.PokemonDAO;
import lombok.RequiredArgsConstructor;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

import java.util.HashMap;
import java.util.Map;

/**
 * Route to handle deleting all of the Pokemon in the database.
 */
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class DeleteAllPokemonRoute implements TemplateViewRoute {

    private final Database database;

    @Override
    public ModelAndView handle(Request request, Response response) {
        database.onDemand(PokemonDAO.class).dropTable();
        Map<String, String> attributes = new HashMap<>();
        attributes.put("message", "The list of Pokemon has been deleted.");
        return new ModelAndView(attributes, "index.ftl");
    }
}
