package routes;

import com.google.inject.Inject;
import dao.Database;
import dao.PokemonDAO;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

import java.util.HashMap;
import java.util.Map;

/**
 * Route to handle deleting a single Pokemon.
 */
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class DeletePokemonRoute implements TemplateViewRoute {

    private final Database database;

    @Override
    public ModelAndView handle(Request request, Response response) {
        if (!StringUtils.isNumeric(request.params(":id"))) {
            return createFailureView("Can't delete \"" + request.params(":id") + "\", it isn't numeric!");
        }
        PokemonDAO dao = database.open(PokemonDAO.class);
        dao.createTableIfNotExists();
        if (dao.getById(Integer.valueOf(request.params(":id"))) == null) {
            dao.close();
            return createFailureView("Can't delete \"" + request.params(":id") + "\", there's nothing to delete!");
        }
        dao.deleteById(Integer.valueOf(request.params(":id")));
        dao.close();
        Map<String, String> attributes = new HashMap<>();
        attributes.put("message", "Pokemon with id " + request.params(":id") + " has been deleted.");
        return new ModelAndView(attributes, "index.ftl");
    }

    private ModelAndView createFailureView(String value) {
        Map<String, String> attributes = new HashMap<>();
        attributes.put("error", value);
        return new ModelAndView(attributes, "index.ftl");
    }
}

