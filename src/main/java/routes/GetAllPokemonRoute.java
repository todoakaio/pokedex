package routes;

import com.google.inject.Inject;
import dao.Database;
import dao.PokemonDAO;
import lombok.RequiredArgsConstructor;
import model.Pokemon;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Route to handle retrieving all of the Pokemon.
 */
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class GetAllPokemonRoute implements TemplateViewRoute {

    private final Database database;

    @Override
    public ModelAndView handle(Request request, Response response) {
        Map<String, List<Pokemon>> attributes = new HashMap<>();
        database.onDemand(PokemonDAO.class).createTableIfNotExists();
        attributes.put("allPokemon", database.onDemand(PokemonDAO.class).getAllPokemon());
        return new ModelAndView(attributes, "allPokemon.ftl");
    }
}
