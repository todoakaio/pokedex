package mapper;

import model.Pokemon;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Implements the ResultSetMapper by mapping values from the ResultSet to Pokemon Objects.
 */
public class PokemonMapper implements ResultSetMapper<Pokemon> {

    @Override
    public Pokemon map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return buildPokemon(resultSet);
    }

    private Pokemon buildPokemon(ResultSet rs) throws SQLException {
        return Pokemon.builder()
                .id(rs.getInt("id"))
                .name(prepareName(rs.getString("name")))
                .height(rs.getInt("height"))
                .weight(rs.getInt("weight"))
                .color(getColorCode(rs.getInt("color_id")))
                .evolves_from_id(rs.getInt("evolves_from_id"))
                .build();
    }

    private String prepareName(String name) {
        String sansDashes = StringUtils.replace(name, "-", " ");
        return WordUtils.capitalize(sansDashes);
    }

    private String getColorCode(int color_id) {
        switch (color_id) {
            case 1:
                return "black";
            case 2:
                return "blue";
            case 3:
                return "brown";
            case 4:
                return "grey";
            case 5:
                return "green";
            case 6:
                return "pink";
            case 7:
                return "purple";
            case 8:
                return "red";
            case 9:
                return "grey";
            case 10:
                return "#A0A000";
            default:
                return "grey";
        }
    }
}
