package controller;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Tests to make sure that the service can start up and take requests.
 */
public class PokedexServerTest {

    @BeforeClass
    public void setUp() throws Exception {
        Injector injector = Guice.createInjector();
        PokedexServer pokedexServer = injector.getInstance(PokedexServer.class);
        pokedexServer.setUpRoutes();
    }

    @Test
    public void testHello() throws Exception {
        HttpResponse<String> response = Unirest.get("http://localhost:5000/hello?q=steve").asString();
        Assert.assertEquals(response.getStatus(), 200);
        Assert.assertEquals(response.getBody(), "Hello World!");
    }

    @Test
    public void testHelloSteve() throws Exception {
        HttpResponse<String> response = Unirest.get("http://localhost:5000/hello/Steve").asString();
        Assert.assertEquals(response.getStatus(), 200);
        Assert.assertEquals(response.getBody(), "Hello, Steve!");
    }
}
