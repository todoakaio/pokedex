<!DOCTYPE html>
<html>
<head>
<#include "header.ftl">
</head>

<body>

<#include "nav.ftl">

<div class="jumbotron text-center">
    <div class="container">
        <h1>Know Your Pokémon!</h1>

        <p>There are now over 700 Pokémon out there. Can you identify them all?</p>
        <a type="button" class="btn btn-default" href="/pokemon"><span class="glyphicon glyphicon-th-list"></span> View the List</a>

        <form action="/search" method="get">
            <div class="input-group">
                <input type="text" class="form-control" name="q" placeholder="Search by Name or ID"/>
            <span class="input-group-btn">
                <button class="btn btn-default" type="submit">Go!</button>
            </span>
            </div>
        </form>
    </div>
</div>
<#if message??>
<div class="container">
    <div class="alert alert-info text-center" role="alert">
    ${message}
    </div>
</div>
</#if>
<#if error??>
<div class="container">
    <div class="alert alert-danger text-center" role="alert">
    ${error}
    </div>
</div>
</#if>
</body>
</html>
