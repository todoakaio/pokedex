<!DOCTYPE html>
<html>
<head>
<#include "header.ftl">
</head>

<body>

<#include "nav.ftl">

<div class="pokemon" style ="/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#7db9e8+0,ff0000+100&amp;0+50,1+100 */
background: -moz-linear-gradient(left,  rgba(125,185,232,0) 0%, rgba(190,93,116,0) 50%, ${pokemon.color} 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(125,185,232,0)), color-stop(50%,rgba(190,93,116,0)), color-stop(100%,${pokemon.color})); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(left,  rgba(125,185,232,0) 0%,rgba(190,93,116,0) 50%,${pokemon.color} 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(left,  rgba(125,185,232,0) 0%,rgba(190,93,116,0) 50%,${pokemon.color} 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(left,  rgba(125,185,232,0) 0%,rgba(190,93,116,0) 50%,${pokemon.color} 100%); /* IE10+ */
background: linear-gradient(to right,  rgba(125,185,232,0) 0%,rgba(190,93,116,0) 50%,${pokemon.color} 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#007db9e8', endColorstr='#ff0000',GradientType=1 ); /* IE6-9 */
">
    <#if pokemon.id < 722 && 0 < pokemon.id>
    <img src="http://git.veekun.com/pokedex-media.git/blob_plain/8571809f9ff0c7ada23c1feb3bc9c1d3f202e093:/pokemon/cropped/${pokemon.id}.png">
    </#if>
    <h1>${pokemon.id}. ${pokemon.name}</h1>

    <h3>Height: ${pokemon.height} Weight:${pokemon.weight}</h3>
    <#if evolves_from??>
        <p>Evolves from <a href="/pokemon/${evolves_from.id}">${evolves_from.name}</a></p>
    </#if>
</div>

<div class="pokemon">

<#if pokemonBefore??>
    <a class="pokemonBefore" href="/pokemon/${pokemonBefore.id}">← ${pokemonBefore.name}</a>
<#else>
    &nbsp;
</#if>
<#if pokemonAfter??>
    <a class="pokemonAfter" href="/pokemon/${pokemonAfter.id}">${pokemonAfter.name} →</a>
</#if>
</div>

</body>
</html>
