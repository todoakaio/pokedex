<!DOCTYPE html>
<html>
<head>
  <#include "header.ftl">
</head>

<body>

  <#include "nav.ftl">

<div class="container">

  <h1>List of Pokemon</h1>
    <ul>
    <#list allPokemon as pokemon>
      <li> ${pokemon.id}. ${pokemon.name} </li>
    </#list>
    </ul>

</div>

</body>
</html>
