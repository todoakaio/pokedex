<!DOCTYPE html>
<html>
<head>
<#include "header.ftl">
</head>

<body>

<#include "nav.ftl">

<div class="edit_pokemon">
    <form action="/pokemon/edit" method="post">
        <div class="input-group col-lg-6">
            <span class="input-group-addon" id="basic-addon1">ID</span>
            <input type="text" class="form-control" aria-describedby="basic-addon1" name="id" required>
        </div>
        <div class="input-group col-lg-6">
            <span class="input-group-addon" id="basic-addon1">Name</span>
            <input type="text" class="form-control" aria-describedby="basic-addon1" name="name" required>
        </div>
        <div class="input-group  col-lg-6">
            <span class="input-group-addon" id="basic-addon1">Height</span>
            <input type="text" class="form-control" aria-describedby="basic-addon1" name="height" required>
        </div>
        <div class="input-group  col-lg-6">
            <span class="input-group-addon" id="basic-addon1">Weight</span>
            <input type="text" class="form-control" aria-describedby="basic-addon1" name="weight" required>
        </div>
        <div class="input-group  col-lg-6">
            <span class="input-group-addon" id="basic-addon1">Color</span>
            <input type="text" class="form-control" aria-describedby="basic-addon1" name="color" required>
        </div>
        <div class="input-group  col-lg-6">
            <span class="input-group-addon" id="basic-addon1">Evolves From ID</span>
            <input type="text" class="form-control" aria-describedby="basic-addon1" name="evolves_from">
        </div>
        <div class="input-group  col-lg-6">
            <span class="input-group-btn">
                <button class="btn btn-default" type="submit">Go!</button>
            </span>
        </div>
    </form>
</div>

</body>
</html>
