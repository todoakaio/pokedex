<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">
                <img alt="Pokedex Demo" src="http://img03.deviantart.net/49cf/i/2011/068/a/c/pokeball_desktop_icon_by_beccerberry-d3b98cf.png" height="20" width="20">
            </a>
        </div>
        <ul class="nav navbar-nav">
            <li>
                <a href="/"><span class="glyphicon glyphicon-home"></span> Home</a>
            </li>
            <li>
                <a href="/pokemon"><span class="glyphicon glyphicon-th-list"></span> List</a>
            </li>
            <li>
                <a href="/pokemon/edit"><span class="glyphicon glyphicon-edit"></span> Edit</a>
            </li>
            <li>
                <a href="/pokemon/delete"><span class="glyphicon glyphicon-trash"></span> Clear</a>
            </li>
            <li>
                <a href="/pokemon/init"><span class="glyphicon glyphicon-repeat"></span> Reset</a>
            </li>
            <li class="navbar-right">
                <a href="/rest"><span class="glyphicon glyphicon-globe"></span> REST API</a>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <form class="navbar-form navbar-right" role="search" action="/search">
                <div class="form-group">
                <#if searchRequest??>
                    <input type="text" class="form-control" value=${searchRequest} name="q">
                <#else>
                    <input type="text" class="form-control" placeholder="Search Pokémon" name="q">
                </#if>
                </div>
                <button type="submit" class="btn btn-default">Go!</button>
            </form>
            <li class="navbar-right">
                <a href="http://git.veekun.com/pokedex-media.git"><span class="glyphicon glyphicon-picture"></span> Media Source</a>
            </li>
            <li class="navbar-right">
                <a href="https://github.com/veekun/pokedex/tree/master/pokedex/data/csv"><span class="glyphicon glyphicon-book"></span> Data Source</a>
            </li>
        </ul>
    </div>
</nav>
