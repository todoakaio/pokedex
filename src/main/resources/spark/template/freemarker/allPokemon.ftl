<!DOCTYPE html>
<html>
<head>
<#include "header.ftl">
</head>

<body>

<#include "nav.ftl">

<div class="allPokemon">

    <h1>List of Pokémon</h1>
    <ul>
    <#list allPokemon as pokemon>
        <li> ${pokemon.id}. <a href="/pokemon/${pokemon.id}" style="color: ${pokemon.color};">${pokemon.name}</a></li>
    </#list>
    </ul>

</div>

</body>
</html>
