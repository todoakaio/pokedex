<!DOCTYPE html>
<html>
<head>
<#include "header.ftl">
</head>

<body>

<#include "nav.ftl">

<div class="allPokemon">

    <h1>REST API</h1>
    <p>You'll need a REST client to do any of the non-GET operations.</p>
    <ul>
        <li> <a href="/rest">GET /rest</a> shows this page</li>
    </ul>
    <p>Working with the List</p>
    <ul>
        <li> <a href="/pokemon">GET /pokemon</a> returns a list of Pokémon</li>
        <li> DELETE /pokemon deletes the list of Pokémon</li>
        <li> <a href="/search?q=pikchu">GET /search?q=pikchu</a> returns search results for the specified query</li>
    </ul>
    <p>Modifying Data</p>
    <ul>
        <li> <a href="/pokemon/edit">GET /pokemon/edit</a> takes you to page where you can edit a Pokémon</li>
        <li> <a href="/pokemon/delete">GET /pokemon/delete</a> deletes the list of Pokémon</li>
        <li> <a href="/pokemon/init">GET /pokemon/init</a> deletes and repopulates the list of Pokémon with a pre-set data file</li>
        <li> POST /pokemon/init</a> deletes and repopulates the list of Pokémon with a pre-set data file</li>
        <li> POST /pokemon/edit</a> Updates/creates a Pokémon with the id specified in the query parameters</li>
    </ul>
    <p>Acting on a specific Pokémon</p>
    <ul>
        <li> <a href="/pokemon/25">GET /pokemon/:id</a> takes you to a page that displays information about a Pokémon</li>
        <li> PUT /pokemon/:id</a> Updates/creates a Pokémon with the id specified in the url path</li>
        <li> DELETE /pokemon/:id</a> deletes the specified Pokémon</li>
    </ul>
    <p>Hello world!</p>
    <ul>
        <li> <a href="/hello">GET /hello</a> prints Hello, World!</li>
        <li> <a href="/hello/name">GET /hello/:name</a> prints Hello, (name)!</li>
    </ul>
    <ul>
    </ul>

</div>

</body>
</html>
