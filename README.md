# Pokemon Demo

A demo app that lets users handle data about Pokémon.

## On the Web

You can check it out here: [Pokemon Demo](https://steve-allred-pokemon-demo.herokuapp.com/), or go straight to the REST API page here: [REST API](https://steve-allred-pokemon-demo.herokuapp.com/rest).
